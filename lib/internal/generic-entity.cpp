// Copyright (C) 2020 Evan La Fontaine
//
// This program is free software: you can redistribute it and/or modify
// it under the terms of the GNU General Public License as published by
// the Free Software Foundation, either version 3 of the License, or
// (at your option) any later version.
//
// This program is distributed in the hope that it will be useful,
// but WITHOUT ANY WARRANTY; without even the implied warranty of
// MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.	See the
// GNU General Public License for more details.

// You should have received a copy of the GNU General Public License
// along with this program.


#include "core/generic-entity.h"


namespace Harmony {
    Internal::Index Entity::Hive::get_id(Internal::Index i) {
        return reinterpret_cast<Internal::Index*>(
                reinterpret_cast<char*>(this) +
                sizeof(*this)
        )[i];
    }
    void Entity::Hive::put_id(Internal::Index i, Internal::Index v) {
        reinterpret_cast<Internal::Index*>(
                reinterpret_cast<char*>(this) +
                sizeof(*this)
        )[i] = v;
    }

    Entity::Hive& Entity::get_hive(Internal::Index hive) {
        return *const_cast<Hive*>(Internal::load<Hive>(0, 0, -1));
    }

    Entity::Hive& Entity::get_hive() { return get_hive(hive); }

    Entity::Entity(Internal::Index hive) : Handle(hive) { indx = get_new_indx(hive); }

    void Entity::destroy() {
        LOCK(global_hive.lock);
        global_hive.unused[global_hive.unused_head++] = indx;
    }

    Internal::Index Entity::get_new_indx(Internal::Index hive) {
        LOCK(get_hive(hive).lock);
        return get_hive(hive).unused_head == 0
             ? get_hive(hive).current_id++
             : get_hive(hive).unused[--get_hive(hive).unused_head];
    }
}
