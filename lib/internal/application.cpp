// Copyright (C) 2020 Evan La Fontaine
//
// This program is free software: you can redistribute it and/or modify
// it under the terms of the GNU General Public License as published by
// the Free Software Foundation, either version 3 of the License, or
// (at your option) any later version.
//
// This program is distributed in the hope that it will be useful,
// but WITHOUT ANY WARRANTY; without even the implied warranty of
// MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.	See the
// GNU General Public License for more details.

// You should have received a copy of the GNU General Public License
// along with this program.


#include "core/application.h"

#include <iostream>
#include <string.h>
#include <cstdlib>
#include <sys/mman.h>
#ifndef NDEBUG
#include <execinfo.h>
#include <signal.h>
#include <stdlib.h>
#include <unistd.h>
#endif


namespace Harmony {
    namespace Internal {
        void Application::operator()() {
            initialise();
            while (running) update();
            decommission();
        }

        void Application::exit() { running = false; }
    }

    void exit() { Internal::application->exit(); }
}

#ifndef NDEBUG
void handler(int sig) {
    void* trace_items[32];
    size_t trace_size = backtrace(trace_items, 32);

    fprintf(stderr, "Error: signal %d:\n", sig);
    backtrace_symbols_fd(trace_items, trace_size, STDERR_FILENO);
    exit(1);
}
#endif

/* We completely override main and call the user defined main (a la SDL2). Unlike SDL this loop is much stricter in
 * structure and is not written by the user, since it does not make sense to do so. This is as the rest of an engine
 * requires a very specific and well defined state (eg. clean memory pages) and the engine already provides hooks to run
 * things before after and during the main game loop (eg. overriding the arg parser) in a safe manner.
 */
int main() {
#ifndef NDEBUG
    signal(SIGSEGV, handler);
#endif

    if (mmap64(
            reinterpret_cast<void*>(Harmony::Internal::Offset::base - Harmony::Internal::Offset::extra),
            Harmony::Internal::Offset::end + Harmony::Internal::Offset::extra,
            PROT_READ | PROT_WRITE,
            MAP_PRIVATE | MAP_ANONYMOUS | MAP_FIXED | MAP_NORESERVE,
            0, 0
    ) == MAP_FAILED) {
        std::cout << strerror(errno) << std::endl;
        return EXIT_FAILURE;
    }

    (*Harmony::Internal::application)();
    return EXIT_SUCCESS;
}
