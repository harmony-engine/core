// Copyright (C) 2020 Evan La Fontaine
//
// This program is free software: you can redistribute it and/or modify
// it under the terms of the GNU General Public License as published by
// the Free Software Foundation, either version 3 of the License, or
// (at your option) any later version.
//
// This program is distributed in the hope that it will be useful,
// but WITHOUT ANY WARRANTY; without even the implied warranty of
// MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.	See the
// GNU General Public License for more details.

// You should have received a copy of the GNU General Public License
// along with this program.


#pragma once

#include "core/application.h"
#include "core/component.h"
#include "core/generic-entity.h"
#include "core/handle.h"
#include "core/specific-entity.h"
#include "core/spinlock.h"
#include "core/system.h"
