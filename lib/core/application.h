// Copyright (C) 2020 Evan La Fontaine
//
// This program is free software: you can redistribute it and/or modify
// it under the terms of the GNU General Public License as published by
// the Free Software Foundation, either version 3 of the License, or
// (at your option) any later version.
//
// This program is distributed in the hope that it will be useful,
// but WITHOUT ANY WARRANTY; without even the implied warranty of
// MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.	See the
// GNU General Public License for more details.

// You should have received a copy of the GNU General Public License
// along with this program.


#pragma once

#include "handle.h"

#include <string>


namespace Harmony {
    namespace Internal {
        class Application {
        public:
            inline void operator()();
            inline void exit();

        protected:
            virtual void initialise() = 0;
            virtual void update() = 0;
            virtual void decommission() = 0;

        private:
            bool running = true;
        };

        struct ApplicationData {
            std::string name;
            struct Version {
                uint16_t major;
                uint16_t minor;
                uint16_t patch;
            } version;
        };

        extern Application* application;
    }
    extern const Internal::ApplicationData application_data;

    void exit();
    template<typename T> T& system();
}
