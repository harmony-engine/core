// Copyright (C) 2020 Evan La Fontaine
//
// This program is free software: you can redistribute it and/or modify
// it under the terms of the GNU General Public License as published by
// the Free Software Foundation, either version 3 of the License, or
// (at your option) any later version.
//
// This program is distributed in the hope that it will be useful,
// but WITHOUT ANY WARRANTY; without even the implied warranty of
// MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.	See the
// GNU General Public License for more details.

// You should have received a copy of the GNU General Public License
// along with this program.


#pragma once

#include <vector>
#include <string>
#include <fstream>


namespace Harmony {
    template<typename T>
    std::vector<T> load_resource(const std::string& module, const std::string& path) {
        std::ifstream file("resources/" + module + "/" + path, std::ios::ate | std::ios::binary);
        if (!file.is_open())
            throw std::runtime_error("Failed to open resource file");

        auto file_size = static_cast<size_t>(file.tellg());
        std::vector<T> buffer(file_size / sizeof(T));
        file.seekg(0);
        file.read(reinterpret_cast<char*>(buffer.data()), file_size);
        file.close();
        return buffer;
    }
}
