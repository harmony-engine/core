// Copyright (C) 2020 Evan La Fontaine
//
// This program is free software: you can redistribute it and/or modify
// it under the terms of the GNU General Public License as published by
// the Free Software Foundation, either version 3 of the License, or
// (at your option) any later version.
//
// This program is distributed in the hope that it will be useful,
// but WITHOUT ANY WARRANTY; without even the implied warranty of
// MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.	See the
// GNU General Public License for more details.

// You should have received a copy of the GNU General Public License
// along with this program.


#pragma once


#include "handle.h"
#include "generic-entity.h"

#include <cassert>
#include <iterator>


namespace Harmony::Internal {
    template<Internal::Index hive>
    class SpecificEntity {
    public:
        static constexpr Index id = hive;
        SpecificEntity() : indx(Entity::get_new_indx(hive)) { }

        struct Iterator {
        public:
            typedef std::input_iterator_tag iterator_category;
            typedef std::ptrdiff_t          difference_type;
            typedef Entity                  value_type;

            value_type operator*() const { return Entity(hive, indx); }
            bool operator==(const Iterator& other) const { return indx == other.indx; }
            bool operator!=(const Iterator& other) const { return !(*this == other); }
            Iterator& operator++() {
                indx++;
                skip();
                return *this;
            }
            Iterator  operator++(int) {
                Iterator tmp = *this;
                ++(*this);
                return tmp;
            }

            Iterator begin() { return Iterator(0); }
            Iterator end() { return Iterator(Entity::get_hive(hive).current_id); }

        private:
            friend SpecificEntity;
            Iterator() : indx(0) { }
            explicit Iterator(Internal::Index indx) : indx(indx) { skip(); }
            void skip() {
            }
            Internal::Index indx;
        };

        static Iterator entities() { return Iterator(); }

        operator Entity() const { return Entity(hive, indx); }
        void destroy() { static_cast<Entity>(*this).destroy(); }

    protected:
        template<typename T> T _get() { return T(hive, indx); }

    private:
        Internal::Index indx;
    };
}
