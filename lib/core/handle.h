// Copyright (C) 2020 Evan La Fontaine
//
// This program is free software: you can redistribute it and/or modify
// it under the terms of the GNU General Public License as published by
// the Free Software Foundation, either version 3 of the License, or
// (at your option) any later version.
//
// This program is distributed in the hope that it will be useful,
// but WITHOUT ANY WARRANTY; without even the implied warranty of
// MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.	See the
// GNU General Public License for more details.

// You should have received a copy of the GNU General Public License
// along with this program.


#pragma once

#include <cstdint>


namespace Harmony::Internal {
    typedef std::int32_t Index;

    namespace Offset {
        typedef std::uint64_t Offset;

        constexpr Offset extra = 4096; // standard page size
        constexpr Offset end   = 1lu << 46lu;
        constexpr Offset base  = 1lu << 44lu;
        constexpr Offset hive  = 1lu << 34lu;
        constexpr Offset field = 1lu << 22lu;
    }

    template<typename T>
    static T* load(const Index hive, const Index offs, const Index indx) {
        return reinterpret_cast<T*>(
            Offset::base +
            Offset::hive  * hive +
            Offset::field * offs +
            sizeof(T) * indx
        );
    }

    class Handle {
    protected:
        Index hive;
        Index indx;

        Handle() = default;
        explicit Handle(Index hive, Index indx = 0);

        template<typename T>
        T* load(const Index offs) const {
            return Harmony::Internal::load<T>(hive, offs, indx);
        }
    };
}
