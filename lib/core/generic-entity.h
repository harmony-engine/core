// Copyright (C) 2020 Evan La Fontaine
//
// This program is free software: you can redistribute it and/or modify
// it under the terms of the GNU General Public License as published by
// the Free Software Foundation, either version 3 of the License, or
// (at your option) any later version.
//
// This program is distributed in the hope that it will be useful,
// but WITHOUT ANY WARRANTY; without even the implied warranty of
// MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.	See the
// GNU General Public License for more details.

// You should have received a copy of the GNU General Public License
// along with this program.


#pragma once

#include "handle.h"
#include "spinlock.h"

#include <cassert>
#include <iostream>


namespace Harmony {
    namespace Internal {
        class Component;
        template<Internal::Index hive_indx> class SpecificEntity;
    }

    class Entity : private Internal::Handle {
    private:
        friend Internal::Component;
        template<Internal::Index> friend class Internal::SpecificEntity;
        using Handle::Handle;

        struct Hive {
            Spinlock lock;
            Internal::Index current_id;
            Internal::Index unused_head;

            Internal::Index get_id(Internal::Index i);
            void put_id(Internal::Index i, Internal::Index v);
            __declspec(property(get=get_id, put=put_id)) Internal::Index unused[];
        };
        static Hive& get_hive(Internal::Index hive);
        Hive& get_hive();
        __declspec(property(get=get_hive)) Hive& global_hive;

    public:
        template<typename T>
        bool has() {
            return Internal::load<bool>(hive, 0, T::type_id);
        }

        template<typename T>
        T get() {
            assert(has<T>());
            return T(hive, indx);
        }

    protected:
        explicit Entity(Internal::Index hive);
        void destroy();

    private:
        static Internal::Index get_new_indx(Internal::Index hive);
    };
}
