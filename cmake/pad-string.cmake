function(pad_string output str padchar length)
    string(LENGTH "${str}" STRLEN)
    math(EXPR STRLEN "${length} - ${STRLEN}")

    if(STRLEN GREATER 0)
        if(${CMAKE_VERSION} VERSION_LESS "3.14")
            unset(PAD)
            foreach(_I RANGE 1 ${STRLEN}) # inclusive
                string(APPEND PAD ${padchar})
            endforeach()
        else()
            string(REPEAT ${padchar} ${STRLEN} PAD)
        endif()
        string(PREPEND str ${PAD})
    endif()

    set(${output} "${str}" PARENT_SCOPE)
endfunction()
