// Copyright (C) 2020 Evan La Fontaine
//
// This program is free software: you can redistribute it and/or modify
// it under the terms of the GNU General Public License as published by
// the Free Software Foundation, either version 3 of the License, or
// (at your option) any later version.
//
// This program is distributed in the hope that it will be useful,
// but WITHOUT ANY WARRANTY; without even the implied warranty of
// MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.	See the
// GNU General Public License for more details.

// You should have received a copy of the GNU General Public License
// along with this program.


#include "printer.h"

#include <iostream>
#include <harmony/core/application.h>

#include "archetypes/generic-message.h"


namespace Harmony::Examples::HelloWorld {
    void Printer::initialise() {
        GenericMessage("Hello, World!");
    }

    void Printer::update(const Message m) {
        std::cout << m.text << std::endl;
    }

    void Printer::post_update() {
        Harmony::exit();
    }
}
